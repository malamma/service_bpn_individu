﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Service_BPN_Individu
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "addDataFJB")]
        string addDataFJB(FJB data);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAllDataFJB")]
        List<FJB> getAllData();

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "getData/{param}")]
        List<FJB> getData(string param);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "updateDataFJB")]
        void updateDataFJB(FJB data);

        [OperationContract]
        [WebInvoke(Method = "DELETE", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "deleteDataFJB")]
        void deleteDataFJB(FJB data);


        //////////////////////////////////////////////////////////////////////////////////

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAllSuratKuasa")]
        List<SuratKuasa> getSuratKuasa();


        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAllKetJB")]
        List<KetJB> getKetJB();


        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAllSporadik")]
        List<Sporadik> getSporadik();


        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAllPBB")]
        List<PBB> getPBB();



        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]

    public class FJB
    {
        private string nik_pemohon, nama_pemohon, alamat_pemohon, letak_tanah, surat_kuasa, keterangan_jb,
            sporadik, pbb;
        private int luas_tanah;
        private DateTime tanggal;

        [DataMember(Order = 1)] public string NIKPemohon { get => nik_pemohon; set => nik_pemohon = value; }

        [DataMember(Order = 2)] public string NamaPemohon { get => nama_pemohon; set => nama_pemohon = value; }

        [DataMember(Order = 3)] public string AlamatPemohon { get => alamat_pemohon; set => alamat_pemohon = value; }

        [DataMember(Order = 4)] public int LuasTanah { get => luas_tanah; set => luas_tanah = value; }

        [DataMember(Order = 5)] public string LetakTanah { get => letak_tanah; set => letak_tanah = value; }

        [DataMember(Order = 6)] public string SuratKuasa { get => surat_kuasa; set => surat_kuasa = value; }

        [DataMember(Order = 7)] public string KeteranganJB { get => keterangan_jb; set => keterangan_jb = value; }

        [DataMember(Order = 8)] public string Sporadik { get => sporadik; set => sporadik = value; }

        [DataMember(Order = 9)] public string PBB { get => pbb; set => pbb = value; }

        [DataMember(Order = 10)] public DateTime Tanggal { get => tanggal; set => tanggal = value; }
    }

    [DataContract]
    public class SuratKuasa
    {
        private string status;
        [DataMember(Order = 1)] public string Status { get => status; set => status = value; }
    }

    [DataContract]
    public class KetJB
    {
        private string status;
        [DataMember(Order = 1)] public string Status { get => status; set => status = value; }
    }

    [DataContract]
    public class Sporadik
    {
        private string status;
        [DataMember(Order = 1)] public string Status { get => status; set => status = value; }
    }

    [DataContract]
    public class PBB
    {
        private string status;
        [DataMember(Order = 1)] public string Status { get => status; set => status = value; }
    }
}
