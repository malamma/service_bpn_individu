﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Service_BPN_Individu
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        SqlConnection conn = new SqlConnection("Data Source=LEGION-Y530;Initial Catalog=bpnApp;User ID=sa;Password=faan.33m");

        public string addDataFJB(FJB data)
        {
            string message = "";
            string query = "insert into first_sertifikat_jb(nik_pemohon, nama_pemohon, alamat_pemohon, luas_tanah, " +
                "letak_tanah, surat_kuasa, keterangan_jb, sporadik, pbb, tanggal) " +
                "values (@nik, @namapemilik, @alamat, @luas, @letak, @sk, @ketjb, @sporadik, " +
                "@pbb, @tanggal)";

            SqlCommand cmd = new SqlCommand(query, conn);

            cmd.Parameters.AddWithValue("@nik", data.NIKPemohon);
            cmd.Parameters.AddWithValue("@namapemilik", data.NamaPemohon);
            cmd.Parameters.AddWithValue("@alamat", data.AlamatPemohon);
            cmd.Parameters.AddWithValue("@luas", data.LuasTanah);
            cmd.Parameters.AddWithValue("@letak", data.LetakTanah);
            cmd.Parameters.AddWithValue("@sk", data.SuratKuasa);
            cmd.Parameters.AddWithValue("@ketjb", data.KeteranganJB);
            cmd.Parameters.AddWithValue("@sporadik", data.Sporadik);
            cmd.Parameters.AddWithValue("@pbb", data.PBB);
            cmd.Parameters.AddWithValue("@tanggal", data.Tanggal);


            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                message = "Query berhasil";
                conn.Close();
            }

            catch (Exception e)
            {
                message = "Koneksi fail" + e.Message;
            }


            return message;
        }

        public void deleteDataFJB(FJB data)
        {
            string query = "delete from first_sertifikat_jb where nik_pemohon = @nik";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@nik", data.NIKPemohon);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public List<FJB> getAllData()
        {
            List<FJB> fjb = new List<FJB>();

            string query = "select * from dbo.first_sertifikat_jb";
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                FJB fjbb = new FJB();

                fjbb.NIKPemohon = sdr[1].ToString();
                fjbb.NamaPemohon = sdr[2].ToString();
                fjbb.AlamatPemohon = sdr[3].ToString();
                fjbb.LuasTanah = int.Parse(sdr[4].ToString());
                fjbb.LetakTanah = sdr[5].ToString();
                fjbb.SuratKuasa = sdr[6].ToString();
                fjbb.KeteranganJB = sdr[7].ToString();
                fjbb.Sporadik = sdr[8].ToString();
                fjbb.PBB = sdr[9].ToString();
                fjbb.Tanggal = DateTime.Parse(sdr[10].ToString());

                fjb.Add(fjbb);
            }
            conn.Close();
            return fjb;
        }

        public List<FJB> getData(string param)
        {
            List<FJB> fjb = new List<FJB>();

            string query = "select * from dbo.first_sertifikat_jb where nik_pemohon like '%" + param + "%' or nama_pemohon like '%" + param + "%'";
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                FJB fjbb = new FJB();
                fjbb.NIKPemohon = sdr[1].ToString();
                fjbb.NamaPemohon = sdr[2].ToString();
                fjbb.AlamatPemohon = sdr[3].ToString();
                fjbb.LuasTanah = int.Parse(sdr[4].ToString());
                fjbb.LetakTanah = sdr[5].ToString();
                fjbb.SuratKuasa = sdr[6].ToString();
                fjbb.KeteranganJB = sdr[7].ToString();
                fjbb.Sporadik = sdr[8].ToString();
                fjbb.PBB = sdr[9].ToString();
                fjbb.Tanggal = DateTime.Parse(sdr[10].ToString());

                fjb.Add(fjbb);
            }
            conn.Close();
            return fjb;
        }

        public List<KetJB> getKetJB()
        {
            List<KetJB> kjb = new List<KetJB>();

            string query = "select * from dbo.keterangan_jb";
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                KetJB kjbb = new KetJB();

                kjbb.Status = sdr[1].ToString();

                kjb.Add(kjbb);
            }
            conn.Close();
            return kjb;
        }

        public List<PBB> getPBB()
        {
            List<PBB> pbb = new List<PBB>();

            string query = "select * from dbo.pbb";
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                PBB pbbb = new PBB();

                pbbb.Status = sdr[1].ToString();

                pbb.Add(pbbb);
            }
            conn.Close();
            return pbb;
        }

        public List<Sporadik> getSporadik()
        {
            List<Sporadik> sporadik = new List<Sporadik>();

            string query = "select * from dbo.sporadik";
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                Sporadik sporadikk = new Sporadik();

                sporadikk.Status = sdr[1].ToString();

                sporadik.Add(sporadikk);
            }
            conn.Close();
            return sporadik;
        }

        public List<SuratKuasa> getSuratKuasa()
        {
            List<SuratKuasa> sk = new List<SuratKuasa>();

            string query = "select * from dbo.surat_kuasa";
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                SuratKuasa skk = new SuratKuasa();

                skk.Status = sdr[1].ToString();

                sk.Add(skk);
            }
            conn.Close();
            return sk;
        }

        public void updateDataFJB(FJB data)
        {
            string query = "update first_sertifikat_jb set nik_pemohon=@nik, nama_pemohon=@namapemilik,alamat_pemohon=@alamat, luas_tanah=@luas, " +
                "letak_tanah=@letak, surat_kuasa=@sk, keterangan_jb=@ketjb, sporadik=@sporadik, pbb=@pbb, tanggal=@tanggal where nik_pemohon=@nik";

            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@nik", data.NIKPemohon);
            cmd.Parameters.AddWithValue("@namapemilik", data.NamaPemohon);
            cmd.Parameters.AddWithValue("@alamat", data.AlamatPemohon);
            cmd.Parameters.AddWithValue("@luas", data.LuasTanah);
            cmd.Parameters.AddWithValue("@letak", data.LetakTanah);
            cmd.Parameters.AddWithValue("@sk", data.SuratKuasa);
            cmd.Parameters.AddWithValue("@ketjb", data.KeteranganJB);
            cmd.Parameters.AddWithValue("@sporadik", data.Sporadik);
            cmd.Parameters.AddWithValue("@pbb", data.PBB);
            cmd.Parameters.AddWithValue("@tanggal", data.Tanggal);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
}
